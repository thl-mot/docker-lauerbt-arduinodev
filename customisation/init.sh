#!/bin/bash

echo "root.sh => install packages"
apt-get -qq update
apt-get install -y git wget xterm ant unzip tar libgtk2.0-0 libgtk-3-0 xz-utils
#apt-get install -y libgtk2.0-0
echo "root.sh => install oracle jdk"

echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee /etc/apt/sources.list.d/webupd8team-java.list
echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
apt-get -qq update
apt-get install -y oracle-java8-installer

update-alternatives --install "/usr/bin/java" "java" "/opt/Oracle_Java/jdk1.8.0_VERSION/bin/java" 1
update-alternatives --install "/usr/bin/javac" "javac" "/opt/Oracle_Java/jdk1.8.0_VERSION/bin/javac" 1
update-alternatives --install "/usr/bin/javaws" "javaws" "/opt/Oracle_Java/jdk1.8.0_VERSION/bin/javaws" 1
update-alternatives --install "/usr/bin/jar" "jar" "/opt/Oracle_Java/jdk1.8.0_VERSION/bin/jar" 1

update-alternatives --set "java" "/opt/Oracle_Java/jdk1.8.0_VERSION/bin/java"
update-alternatives --set "javac" "/opt/Oracle_Java/jdk1.8.0_VERSION/bin/javac"
update-alternatives --set "javaws" "/opt/Oracle_Java/jdk1.8.0_VERSION/bin/javaws"
update-alternatives --set "jar" "/opt/Oracle_Java/jdk1.8.0_VERSION/bin/jar" 

apt-get clean
apt-get purge

mkdir /opt
cd /opt

echo "root.sh => getting arduino-ide..."
wget --quiet https://downloads.arduino.cc/arduino-1.8.5-linux64.tar.xz -O arduino1.8.5.tar.xz

echo "root.sh => getting sloeber..."
wget --quiet https://github.com/Sloeber/arduino-eclipse-plugin/releases/download/V4_1/V4.1_linux64.2017-05-25_16-52-16.tar.gz -O sloeber4.1.tar.gz

tar -xpvf arduino1.8.5.tar.xz
tar -xpvf sloeber4.1.tar.gz

rm *.tar.xz
rm *.tar.gz

cp -r /opt/customisation/.settings /opt/sloeber/configuration/.

echo "init.sh => ESP32 support"


#echo "init.sh => Create directory /volumes/video and set groups and  user"
#mkdir -p /volumes/video




